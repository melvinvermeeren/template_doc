# Doc Template

**2020-05-17: This template is EOL, use the upstream "C Template" instead.**

Magical template for documentation projects, improves quality of life.

Released into the public domain so you don't have to bother with crediting this.

A list of software used can be found in this file. For absolute minimum
requirements for build steps refer to `cicd/docker_targets/*/bootstrap.sh`.

Further details and diagrams are provided in the generated sphinx-based html
documentation:
* [Latest release tag](https://template.doc.mel.vin/doc)
* [Development version](https://doc.mel.vin/template/doc/review-master-3x73j2)

The official project home and all official mirrors:
* Home: https://git.mel.vin/template/doc
* GitLab: https://gitlab.com/melvinvermeeren/template_doc
* GitHub: https://github.com/melvinvermeeren/template_doc

## Build

* [cmake](https://cmake.org/)
  * simplified complicated build system

## Documentation

* a nice directory layout that is easily extended
* [Sphinx](http://www.sphinx-doc.org/en/stable/)
  * for all other documentation in wonderful reStructuredText

## CI CD

* [GitLab CI/CD](https://docs.gitlab.com/ce/ci/)
  * integrated with the entire very well designed workflow
* [docker](https://git.mel.vin/cicd/docker)
  * easy building and deployment of docker images

## Tips

* [conf/vim](https://git.mel.vin/conf/vim)
  * the brilliant cult again creates the superior user experience
* [spacemacs](http://spacemacs.org/)
  * the evil church can never escape from their bloated foundations

*Note: The authors may have been slightly biased when this section was written.*
